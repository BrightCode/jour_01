#ifndef BC_H_
# define BC_H_

void	bc_write_char(char);
void	bc_write_str(char *);
void	bc_write_nb(int);
char	*bc_get_line(int);
char	bc_get_char();


#endif /* !BC_H_ */
